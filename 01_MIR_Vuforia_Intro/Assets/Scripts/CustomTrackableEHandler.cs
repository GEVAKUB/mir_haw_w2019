﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class CustomTrackableEHandler : MonoBehaviour, ITrackableEventHandler
{

    private TrackableBehaviour mTrackableBehaviour;
    private AudioSource audioSource;
    private AudioLowPassFilter alpf;
    private bool isTracked = false;

    // Start is called before the first frame update
    void Start() {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        audioSource = GetComponent<AudioSource>();
        alpf = GetComponent<AudioLowPassFilter>();

        if (mTrackableBehaviour) {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED ||
            newStatus == TrackableBehaviour.Status.TRACKED)
        {
            isTracked = true;
            if (!audioSource.isPlaying)
                audioSource.Play();
        }

        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                    newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            isTracked = false;
            if (audioSource.isPlaying)
                audioSource.Stop();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isTracked)
        {
            alpf.cutoffFrequency = 500.0f * Camera.main.transform.position.magnitude;
            //Debug.Log("Abstand: " + Camera.main.transform.position.magnitude);
        }
    }
}
