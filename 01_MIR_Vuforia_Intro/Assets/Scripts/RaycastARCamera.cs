﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastARCamera : MonoBehaviour
{
    private RaycastHit hit;
    private bool hitObject = false;
    private Material materialColored;
    private Color originalColor;

    // Start is called before the first frame update
    void Start()
    {
        materialColored = new Material(Shader.Find("Transparent/Diffuse"));
        originalColor = this.GetComponent<Renderer>().material.color;

    }

    private void ChangeMaterialColor(GameObject target, Color newColor)
    {
        materialColored.color = newColor;
        target.GetComponent<Renderer>().material = materialColored;
    }

    // Update is called once per frame
    void Update()
    {
        Transform cam = Camera.main.transform;
        Ray ray = new Ray(cam.position, cam.forward);

        Debug.DrawRay(ray.origin, ray.direction * 1000.0f, Color.green, 1, false);

        hitObject = false;
        if (Physics.Raycast(ray, out hit, 1000.0f))
        {
            Debug.Log("hitting: " + hit.transform.name);
            hitObject = true;

        }
        
        ChangeMaterialColor(this.gameObject, originalColor);

        if (hitObject)
        {
            if (hit.transform.name == this.gameObject.transform.name)
                ChangeMaterialColor(this.gameObject, Color.green);
        }

    }


   /* private void ChangeMaterialColor (GameObject target, Color newColor)
    {
        materialColored.color = newColor;
        target.GetComponent<Renderer>().material = materialColored;
    }*/
}
