﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class CustomTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
{
    private TrackableBehaviour mTrackableBehaviour;
    private bool isTracked = false;

    private AudioSource audioSource;
    private AudioLowPassFilter alpf;

    // Start is called before the first frame update
    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        audioSource = GetComponent<AudioSource>();
        alpf = GetComponent<AudioLowPassFilter>();

        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

      
    }

    // Update is called once per frame
    void Update()
    {
        if (isTracked)
            Debug.Log("Distance: " + Camera.main.transform.position.magnitude.ToString("F2"));

        if (audioSource.isPlaying)
            alpf.cutoffFrequency = Camera.main.transform.position.magnitude * 5000.0f;


    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            //Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
            isTracked = true;

            if (!audioSource.isPlaying)
                audioSource.Play();
        }

        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
            newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            //Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
            isTracked = false;

            if (audioSource.isPlaying)
                audioSource.Stop();
        }
    }
}
