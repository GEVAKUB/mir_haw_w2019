﻿using System;
using UnityEngine;
using UnityEngine.Networking;

public class AndroidMessage : MessageBase
{
    public Vector3 acc;
    public Vector3 rotationRateUnbiased;
    public Vector3 rotationRate;
    public Vector3 gravity;
    public Vector3 userAcceleration;
    public Quaternion attitude;
    public bool enabled;


}