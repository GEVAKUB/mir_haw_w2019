﻿using System;
using UnityEngine;
using UnityEngine.Networking;


public class Client
{
    public int port = 9999;
    public string ip = "192.168.11.8";

    // The id we use to identify our messages and register the handler
    public short messageID = 1000;
    public bool isConnected = false;

    // The network client
    NetworkClient client;

    
    public Client()
    {
        CreateClient();
    }

    public Client(String ip, int port)
    {
        this.ip = ip;
        this.port = port;

        CreateClient();
    }

    void CreateClient()
    {
        var config = new ConnectionConfig();

        // Config the Channels we will use
        config.AddChannel(QosType.ReliableFragmented);
        config.AddChannel(QosType.UnreliableFragmented);
        

        // Create the client ant attach the configuration
        client = new NetworkClient();
        client.Configure(config, 1);

        // Register the handlers for the different network messages
        RegisterHandlers();

        // Connect to the server
        client.Connect(ip, port);
    }

    // Register the handlers for the different message types
    void RegisterHandlers()
    {
        // Unity have different Messages types defined in MsgType
        client.RegisterHandler(messageID, OnMessageReceived);
        client.RegisterHandler(MsgType.Connect, OnConnected);
        client.RegisterHandler(MsgType.Disconnect, OnDisconnected);

    }

    void OnConnected(NetworkMessage message)
    {
        this.isConnected = true;
        // Do stuff when connected to the server

    }
    
    void OnDisconnected(NetworkMessage message)
    {
        this.isConnected = false;
        client.Connect(ip, port);


        // Do stuff when disconnected to the server
    }

    // Message received from the server
    void OnMessageReceived(NetworkMessage netMessage)
    {
        // You can send any object that inherence from MessageBase
        // The client and server can be on different projects, as long as the MyNetworkMessage or the class you are using have the same implementation on both projects
        // The first thing we do is deserialize the message to our custom type
        // var objectMessage = netMessage.ReadMessage<AndroidMessage>();

        // Debug.Log("Message received: " + objectMessage.acc);
        // Debug.Log("Message received: " + objectMessage.attitude);
    }
    
    public void sendMessage (int messageId, AndroidMessage msg)
    {
        client.Send(messageID, msg);
    }

    public void connect (String ipAddress)
    {
        client.Connect(ipAddress, port);
    }

    public void disconnect()
    {
        Debug.Log("Disconnect");
        client.Disconnect();
        this.isConnected = false;       
    }


}
