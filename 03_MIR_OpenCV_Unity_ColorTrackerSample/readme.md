# ColorTracker Sample

## Unity Version 
2018.4.11 (LTS)

## Benötigte Assets aus AssetStore
OpenCVplusUnity (https://assetstore.unity.com/packages/tools/integration/opencv-plus-unity-85928)
Das Asset ist frei verfügbar.

## Erläuterung
Das Projekt muss auf dem Telefon ausgeführt werden und funktioniert **nicht** im PlayMode!!!!

Alles was notwendig ist (außer dem Asset) befindet sich im Ordner ColorTracking unter Assets in der Projektstruktur. Der Order enthält die Besipeilszene sowie das Script für das ColorTracking.

In der Beispielszene wird ein Marker getrackt. Gleichzeitig kann ein rotes Objekt (ich habe eine rote Pulmoll Dose verwendet) getrackt werden. Das zu trackende Objekt hält das Script ColorTracker. Im Inspektor dieses Skripts wird der HSV Bereich angegeben, welcher getrackt werden soll. Weitere Infos zum HSV Farbraum findet ihr hier: https://de.wikipedia.org/wiki/HSV-Farbraum.

Getestet habe ich das ganze auf einem Samsung S7 und Alcatel 3V.